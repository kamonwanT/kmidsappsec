package com.app.mercedes.nfc;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private NdefMessage mNdefPushMessage;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    private static final int REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        StrictMode.setThreadPolicy( new StrictMode.ThreadPolicy.Builder().permitAll().build() );
        setContentView( R.layout.activity_main );

        getData( getIntent() );
        verifyPermissions();
        mAdapter = NfcAdapter.getDefaultAdapter( this );
        if (mAdapter == null) {
        }
        mPendingIntent = PendingIntent.getActivity( this, 0, new Intent( this, getClass() ).addFlags( Intent.FLAG_ACTIVITY_SINGLE_TOP ), 0 );
    }



    @Override
    protected void onNewIntent(Intent intent) {
        setIntent( intent );
        getData( intent );
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter != null) {
            if (mAdapter.isEnabled()) {
                mAdapter.enableForegroundDispatch( this, mPendingIntent, null, null );
                Log.d( "Test", "Must stop here" );
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdapter != null) {
            mAdapter.disableForegroundDispatch( this );
        }
    }

    private void getData(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals( action )
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals( action )
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals( action )) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra( NfcAdapter.EXTRA_NDEF_MESSAGES );
            NdefMessage[] msgs;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                    Toast.makeText( this, msgs[i].toString(), Toast.LENGTH_SHORT ).show();
                }
            } else {
                byte[] empty = new byte[0];
                byte[] id = intent.getByteArrayExtra( NfcAdapter.EXTRA_ID );
                Parcelable tag = intent.getParcelableExtra( NfcAdapter.EXTRA_TAG );
                Toast.makeText( this, dumpTagData( tag ), Toast.LENGTH_LONG ).show();
                SharedPreferences.Editor editor = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE ).edit();
                editor.putString( "NFC", dumpTagData( tag ) );
                editor.apply();
            }
            Intent i = new Intent( MainActivity.this, PageActivity.class );
            startActivity( i );
        }
    }

    private String dumpTagData(Parcelable p) {
        StringBuilder sb = new StringBuilder();
        Tag tag = (Tag) p;
        byte[] id = tag.getId();
        sb.append( getDec( id ) );
        return sb.toString();
    }

    private String getHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append( '0' );
            sb.append( Integer.toHexString( b ) );
            if (i > 0) {
                sb.append( " " );
            }
        }
        return sb.toString();
    }

    private long getDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length - 1; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    private long getReversed(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = bytes.length - 1; i >= 0; --i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    private void verifyPermissions() {
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};
        if (ContextCompat.checkSelfPermission( this.getApplicationContext(),
                permissions[0] ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission( this.getApplicationContext(),
                permissions[1] ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission( this.getApplicationContext(),
                permissions[2] ) == PackageManager.PERMISSION_GRANTED) {
        } else {
            ActivityCompat.requestPermissions( MainActivity.this, permissions, REQUEST_CODE );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        verifyPermissions();
    }

}