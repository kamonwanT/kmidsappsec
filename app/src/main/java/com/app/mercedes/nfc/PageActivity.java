package com.app.mercedes.nfc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class PageActivity extends AppCompatActivity {
    Button btnOut;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.page_inout );
        btnOut = (Button) findViewById( R.id.out );
        btnOut.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hh = new Intent( PageActivity.this, ShowOutActivity.class );
                startActivity( hh );
            }
        } );
    }

    public void onOpenCAmera(View view) {
        Intent intent = new Intent( this, CameraActivity.class );
        this.startActivity( intent );
    }
}